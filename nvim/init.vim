call plug#begin('~/.config/nvim/plugged')

" CodeStats plugin
Plug 'https://gitlab.com/code-stats/code-stats-vim.git'

" If you want a nice status line in Vim
Plug 'vim-airline/vim-airline'

" use nvim as a ide
Plug 'preservim/nerdtree'

call plug#end()

"REQUIRED for CodeStats: set your API key
let g:codestats_api_key = ''
" REQUIRED for vim-airline: configure vim-airline to display status
let g:airline_section_x = airline#section#create_right(['tagbar', 'filetype', '%{CodeStatsXp()}'])

" load nerdtree automatically
autocmd vimenter * NERDTree
