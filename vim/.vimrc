call plug#begin()

" vim plugin for vim-airline
Plug 'vim-airline/vim-airline'

" codestats plugin
Plug 'https://gitlab.com/code-stats/code-stats-vim.git'


call plug#end()

" codestats' conf

" REQUIRED for codestats: set your API key
 let g:codestats_api_key = ''

 let g:airline_section_x = airline#section#create_right(['tagbar', 'filetype', '%{CodeStatsXp()}'])

